import axios from 'axios'
import {getLanguageFromURL, prepareLocaleData} from './modules/common/tools/Internationalization'
import momentLocalizer from 'react-widgets/lib/localizers/moment'
import {Moment} from './modules/common/tools/momentLocalized'
import AuthService from './modules/security/services/AuthService'
import {config} from './config/configureSettings'
import {pushHistory} from './modules/common/tools/HistoryTools'
import {getPath} from './modules/common/tools/URLTools'

const auth = new AuthService(config.security.clientId, config.security.secret, config.sanderusBackEndHost)

export default () => {
    Object.assign(axios.defaults, {})
    momentLocalizer(Moment)
    prepareLocaleData()

    if (auth.loggedIn()) {
        auth.setAxiosDefaults()
    }

    axios.interceptors.response.use(
        response => response,
        (error) => {
            const originalRequest = error.config

            if (error.response.status === 401 && !originalRequest._retry) {
                originalRequest._retry = true
                console.log('XXX 1')

                return auth.refresh({
                    host: config.sanderusBackEndHost,
                    callback: ({data, error}) => {
                        if (!error) {
                            console.log('XXX 2')
                            originalRequest.headers['Authorization'] = data.token_type + ' ' + data.access_token
                            axios(originalRequest)
                        } else {
                            console.log('XXX 3')
                            auth.logout()
                            pushHistory(getPath(getLanguageFromURL() + '/login'))
                        }
                    }
                })
            } else {
                console.log('XXX 4')
                return Promise.reject(error)
            }
        }
    )
}
