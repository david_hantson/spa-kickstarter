import React from 'react'
import {IndexRedirect, Route} from 'react-router'
import App from './app'
import {config} from './config/configureSettings'

import {defaultLanguage, supportedLanguages} from './modules/common/tools/Internationalization'
import LoginContainer from './modules/security/containers/LoginContainer'
import AuthService from './modules/security/services/AuthService'

const auth = new AuthService(config.security.clientId, config.security.secret, config.sanderusBackEndHost)


const requireAuth = (language) => {
    return (nextState, replace) => {
        if (!auth.loggedIn()) {
            replace({ pathname: '/' })
        }
    }
}

/*  onEnter={requireAuth(language)}  */
const translatedRoutes = (language) => (
    <Route path={language + '/'} key={language}>
        <IndexRedirect to={'login'}/>
        <Route path="login" component={LoginContainer}/>
    </Route>
)


const routes = (
    <Route path={config.basePath} component={App} auth={auth}>
        <IndexRedirect to={defaultLanguage + '/login'}/>
        {supportedLanguages.map(language => translatedRoutes(language))}
        {/*<Route path="*" onEnter={userRedirect} />*/}
    </Route>
)

export default routes
