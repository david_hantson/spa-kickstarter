import React, {Component} from 'react'
import {connect} from 'react-redux'
import Notification from '../components/Notification'
import {bindActionCreators} from 'redux'
import {removeNotification, startClosingNotification} from '../redux/actions/NotificationActions'
import ui from 'redux-ui'
import StoreCollection from '../models/StoreCollection'

const fadeOutAnimationDuration = 1000

export const mapStateToProps = (state) => {
    return {
        notifications: new StoreCollection(state.global.notifications)
    }
}

export const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        startClosingNotification,
        removeNotification
    }, dispatch)
}

@ui({
    key: 'notifications',
    state: {
        timers: []
    }
})
@connect(
    mapStateToProps,
    mapDispatchToProps
)
export default class NotificationsContainer extends Component {

    closeNotificationAfterTimer({id, delay}) {
        setTimeout(this.closeNotification.bind(this, {id}), delay)
    }

    closeNotification({id}) {
        if (this.props.notifications.has(id) && !this.props.notifications.find(id).isClosing) {
            this.props.startClosingNotification({id})
            setTimeout(this.removeNotification.bind(this, {id}), fadeOutAnimationDuration)
        }
    }

    removeNotification({id}) {
        this.props.removeNotification({id})
        this.removeFromTimers(id)
    }

    addRemoveNotificationTimer(notification) {
        if (this.props.ui.timers.indexOf(notification.id) === -1) {
            let timers = [...this.props.ui.timers, notification.id]
            this.props.updateUI('timers', timers)
            this.closeNotificationAfterTimer({
                id: notification.id,
                delay: (notification.duration * 1000) - fadeOutAnimationDuration
            })
        }
    }

    removeFromTimers(notificationId) {
        let timers = [...this.props.ui.timers]
        timers.splice(timers.indexOf(notificationId), 1)
        this.props.updateUI('timers', timers)
    }

    updateTimers() {
        this.props.notifications.loop((notification) => {
            if (notification.duration) this.addRemoveNotificationTimer(notification)
        })
    }

    componentDidUpdate() {
        this.updateTimers()
    }

    render() {
        return (
            <div className="global_messages">
                {this.props.notifications.map((notification, index) => {
                    return <Notification key={index} {...notification}
                                         onClose={this.closeNotification.bind(this)}/>
                })}
            </div>
        )
    }
}

