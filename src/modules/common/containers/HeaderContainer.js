import React, {Component} from 'react'
import {Header} from '../components/Header'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {supportedLanguages} from '../tools/Internationalization'
import {switchLanguage} from '../redux/actions/LocalesActions'


export const mapStateToProps = (state) => {
    return {
        activeLocale: state.global.locales.lang
    }
}

export const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        switchLanguage
    }, dispatch)
}

@connect(
    mapStateToProps,
    mapDispatchToProps
)
export default class HeaderContainer extends Component {
    changeLanguage(language) {
        this.props.switchLanguage(language)
    }

    render() {
        return (
            <Header
                activeLocale={this.props.activeLocale}
                locales={supportedLanguages}
                changeLocale={this.changeLanguage.bind(this)}
                auth={this.props.auth}
            />
        )
    }
}
