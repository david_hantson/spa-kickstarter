import React from 'react'
import {DatePicker, SelectField, TextField} from 'material-ui'
import {getDateTimeFormat, getLanguageFromURL} from './Internationalization'
import {camelcaseToDashSeparated} from './StringTools'
import {injectIntl} from 'react-intl'

const filterTranslationAttributes = (attributes) => {
    let filteredAttributes = {}

    for (let key in attributes) {
        if (key !== 'intl' && key !== 'translated') {
            filteredAttributes[key] = attributes[key]
        }
    }

    return filteredAttributes
}


export const renderTextField = injectIntl(
    ({input, label, type, meta: {touched, error, warning}, ...custom, intl, translated = true}) => {
        let strLabel = translated ? intl.formatMessage({id: label}) : label

        return (<TextField
            hintText={strLabel}
            floatingLabelText={strLabel}
            errorText={touched && error}
            errorStyle={{float: 'left'}}
            fullWidth={true}
            id={'mui-' + camelcaseToDashSeparated(input.name)}
            type={type}
            {...input}
            {...filterTranslationAttributes(custom)}
        />)
    }
)

export const renderDatePicker = injectIntl(
    ({input, label, type, meta: {touched, error, warning}, ...custom, intl, translated = true}) => {
        let strLabel = translated ? intl.formatMessage({id: label}) : label

        const onChange = (evt, date) => {
            if (input.onChange) {
                input.onChange(date)
            }
        }

        return (
            <DatePicker hintText={strLabel}
                        container="inline"
                        mode="landscape"
                        errorText={touched && error}
                        errorStyle={{float: 'left'}}
                        fullWidth={true}
                        textFieldStyle={{marginTop: '24px'}}
                        onChange={onChange.bind(this)}
                        DateTimeFormat={Intl.DateTimeFormat}
                        id={'mui-' + camelcaseToDashSeparated(input.name)}
                        {...filterTranslationAttributes(custom)}
                        okLabel={intl.formatMessage({id: 'app.forms.date_picker.label.ok'})}
                        cancelLabel={intl.formatMessage({id: 'app.forms.date_picker.label.cancel'})}
                // locale={getLanguageFromURL()}
            />
        )
    }
)

export const renderSelectField = injectIntl(
    ({input, label, meta: {touched, error}, children, ...custom, intl, translated = true}) => {
        let strLabel = translated ? intl.formatMessage({id: label}) : label

        return (
            <SelectField
                floatingLabelText={strLabel}
                errorText={touched && error}
                fullWidth={true}
                id={'mui-' + camelcaseToDashSeparated(input.name)}
                onChange={(event, index, value) => input.onChange(value)}
                name={input.value}
                value={input.value}
                children={children}
                {...filterTranslationAttributes(custom)}
                style={{...custom.style,
                    marginTop: '0'
                }}
            />
        )
    }
)
