export const leftPad = (value, length, padString) => {
    let str = value.toString()
    while (str.length < length)
        str = padString + str
    return str
}

