import moment from 'moment'
moment.locale('be-nl')

export const Moment = moment
export const technicalDateFormat = 'YYYY-MM-DD'