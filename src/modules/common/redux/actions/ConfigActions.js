import {UPDATE_CONFIG} from './types/ConfigTypes'

function updateConfigAction(payload) {
    return {
        type: UPDATE_CONFIG,
        payload
    }
}


export const updateConfig = (config) => {
    return dispatch => {
        dispatch(updateConfigAction(config))
    }
}

