import {CLOSE_NOTIFICATION, NEW_NOTIFICATION, REMOVE_NOTIFICATION} from './types/NotificationTypes'

export const newNotification = (options) => {
    return dispatch => {
        dispatch({
            type: NEW_NOTIFICATION,
            payload: {
                ...options
            }
        })
    }
}

export const startClosingNotification = ({id}) => {
    return dispatch => {
        dispatch({
            type: CLOSE_NOTIFICATION,
            payload: {
                id,
                isClosing: true
            }
        })
    }
}

export const removeNotification = ({id}) => {
    return dispatch => {
        dispatch({
            type: REMOVE_NOTIFICATION,
            payload: {
                id
            }
        })
    }
}