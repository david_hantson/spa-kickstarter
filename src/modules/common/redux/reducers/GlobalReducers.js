import {combineReducers} from 'redux'
import {notifications} from './NotificationReducers'
import {config} from './ConfigReducers'
import {locales} from './LocalesReducer'

export const global = combineReducers({
    notifications,
    config,
    locales
})

export default global
