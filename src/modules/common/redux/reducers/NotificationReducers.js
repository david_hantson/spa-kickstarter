import {CLOSE_NOTIFICATION, NEW_NOTIFICATION, REMOVE_NOTIFICATION} from '../actions/types/NotificationTypes'
import {guid} from '../../tools/GUIDTools'

const initialState = {
    byAggregateId: {},
    allAggregateIds: []
}

const defaultNotificationValues = {
    isClosing: false
}

export const notifications = (state = initialState, action) => {
    let notification, notificationExists, newState

    switch (action.type) {
        case NEW_NOTIFICATION:
            let notificationId = guid()
            notification = {}
            notification[notificationId] = {...defaultNotificationValues, id:notificationId, ...action.payload}

            return {
                ...state,
                byAggregateId: {
                    ...state.byAggregateId, ...notification
                },
                allAggregateIds: [...state.allAggregateIds, notificationId]
            }

        case CLOSE_NOTIFICATION:
            notification = {}
            notification[action.payload.id] = {...state.byAggregateId[action.payload.id], ...action.payload}

            return {...state, byAggregateId: {...state.byAggregateId, ...notification}}

        case REMOVE_NOTIFICATION:
            let newState
            notification = action.payload
            notificationExists = (state.allAggregateIds.indexOf(notification.id) !== -1)

            if (notificationExists) {
                let byAggregateId = Object.keys(state.byAggregateId)
                    .filter(key => key !== notification.id)
                    .reduce((obj, key) => {
                        obj[key] = state.byAggregateId[key]
                        return obj
                    }, {})
                let allAggregateIds = state.allAggregateIds.filter( id => id !== notification.id)

                newState = {
                    ...state,
                    byAggregateId,
                    allAggregateIds
                }
            } else {
                newState = state
            }

            return newState
        default:
            return state
    }
}
