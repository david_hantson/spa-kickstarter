import {UPDATE_CONFIG} from '../actions/types/ConfigTypes'
import {default as runtimeConfig} from '../../../../config/runtime.json'

const initialState = runtimeConfig

export const config = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_CONFIG:
            return {
                ...state,
                ...action.payload
            }
        default:
            return state
    }
}
