import React, {PropTypes} from 'react'
import noop from '../tools/noop'
import {FlatButton} from 'material-ui'
import {injectIntl} from 'react-intl'

export const ButtonTypes = {
    PRIMARY: 'primary',
    SECONDARY: 'secondary'
}

const buttonStyleCommon = {
    border: '2px solid',
    height: '42px',
    lineHeight: '38px',
    borderRadius: '0px'
}

const buttonStylePrimary = {
    ...buttonStyleCommon,
    backgroundColor: '#fff',
    borderColor: '#ec7814',
    color: '#ec7814',
    iconColor: '#ff0014'
}

const buttonDisabledStylePrimary = {
    ...buttonStyleCommon,
    backgroundColor: '#fff',
    borderColor: '#ffd5b0',
    color: '#ffd7b4',
    iconColor: '#ffbeb0'
}

const buttonStyleSecondary = {
    ...buttonStyleCommon,
    backgroundColor: '#fff',
    borderColor: '#ccc',
    color: '#ccc'
}

const buttonDisabledStyleSecondary = {
    ...buttonStyleCommon,
    backgroundColor: '#fff',
    borderColor: '#dedede',
    color: '#dedede'
}


const Button = ({
                    className = '',
                    text = '',
                    styleType = ButtonTypes.PRIMARY,
                    type = 'button',
                    onClick = noop,
                    translated = true,
                    style = {},
                    disabled = false,
                    intl
                }) => {

    let buttonClass = className + ' button ' + styleType
    let buttonStyle
    let rippleColor

    switch (styleType) {
        case ButtonTypes.PRIMARY:
            buttonStyle = {...buttonStylePrimary, ...style}
            if (disabled) buttonStyle = {...buttonStyle, ...buttonDisabledStylePrimary}
            rippleColor = '#ec7814'
            break
        case ButtonTypes.SECONDARY:
            buttonStyle = {...buttonStyleSecondary, ...style}
            if (disabled) buttonStyle = {...buttonStyle, ...buttonDisabledStyleSecondary}
            rippleColor = '#cccccc'
    }

    let label

    if (translated)
        label = intl.formatMessage({id: text})
    else
        label = text

    return (
        <FlatButton
            type={type}
            className={buttonClass}
            label={label}
            onClick={onClick}
            style={buttonStyle}
            rippleColor={rippleColor}
            disabled={disabled}
        />)
}

Button.propTypes = {
    className: PropTypes.string,
    text: PropTypes.string,
    styleType: PropTypes.string,
    type: PropTypes.string,
    onClick: PropTypes.func,
    translated: PropTypes.bool,
    style: PropTypes.object,
    disabled: PropTypes.bool
}

export default injectIntl(Button)
