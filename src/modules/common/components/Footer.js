import React from 'react'
import {FormattedMessage} from 'react-intl'

export default class Footer extends React.Component {
    render() {
        return (
            <footer className="footer">
                <span className="company">IPC - International Post Corporation</span>
                <ul className="links">
                    <li className="link"><FormattedMessage id="app.footer.navigation.contact"/></li>
                    <li className="link">Vacatures</li>
                    <li className="link">FAQ</li>
                    <li className="link">Tevreden?</li>
                </ul>
            </footer>
        )
    }
}
