import React from 'react'
import {INFO} from './NotificationTypes'
import {Icon} from './Icons'

const typeDetails = {
    INFO: {
        icon: 'info'
    },
    ERROR: {
        icon: 'bug'
    },
    WARNING: {
        icon: 'exclamation-triangle'
    },
    SUCCESS: {
        icon: 'check'
    }
}



const Notification = ({
                          id,
                          type = INFO,
                          text = '',
                          duration,
                          onClose = () => {
                          },
                          isClosing = false
                      }) => {

    let hasDuration = (duration !== undefined)

    let isClosingClass = ''
    if (isClosing) isClosingClass = ' is-closing'

    let durationClassName = ''
    if (!isClosing && hasDuration) durationClassName = ' duration-' + duration

    let timedClassName = ''
    if (hasDuration) timedClassName = ' timed'

    let icon = typeDetails[type].icon

    return (
        <div className={'message ' + type.toLowerCase() + timedClassName + durationClassName + isClosingClass}>
            <span className="message-icon"><Icon name={icon} fixedWidth={true}/></span>
            <span className="text-container">
                <span className="text">{text}</span>
                <div className="progress-container"><div className="message-progress"></div></div>
            </span>
            <span className="close-icon" onClick={onClose.bind(this, {id})}><Icon name="times"/></span>
        </div>)
}

export default Notification
