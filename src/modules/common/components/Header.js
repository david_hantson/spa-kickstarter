import React from 'react'
import {getPath} from '../tools/URLTools'
import {Icon, Stack} from './Icons'
import noop from '../tools/noop'
import Navigation from './Navigation'

export const Header = ({
                           locales = [],
                           activeLocale = '',
                           changeLocale = noop,
                           auth = {}
                       }) => {
    const onClickChangeLanguage = (locale) => {
        changeLocale(locale)
    }

    return (
        <div id='header'>
            {/*<img className="logo" src={getPath('images/app/sanderus_logo.jpeg')}/>*/}
            <ul className="language-picker">
                {locales.map(locale => {
                    let language = locale.substring(0, 2)
                    let activeLocaleClass = activeLocale === locale ? ' active' : ''

                    return (
                        <li key={locale}
                            className={'choice' + activeLocaleClass}
                            onClick={onClickChangeLanguage.bind(this, locale)}
                        >
                            <span className="text">{language}</span>
                            <Icon name="circle" className="background"/>
                        </li>
                    )
                })}
            </ul>
            <Navigation auth={auth} language={activeLocale}/>
        </div>
    )
}
