import React, {PropTypes} from 'react'
import AuthService from '../../security/services/AuthService'
import {Link} from 'react-router'
import {getPath, getURLPath} from '../tools/URLTools'
import {FormattedMessage} from 'react-intl'
import {pushHistory} from '../tools/HistoryTools'


const Navigation = ({
                        auth,
                        language
                    }) => {

    const navItems = [
        <li key="logout" className="nav-item">
            <Link onClick={() => {
                auth.logout()
                pushHistory(getPath(language + '/login'))
            }}>
                <FormattedMessage id="app.header.navigation.logout"/>
            </Link>
        </li>
    ]

    let visibleNavItems

    if (auth.loggedIn()) {
        visibleNavItems = navItems
    }

    return (
        <div className="navigation">
            <ul>
                {visibleNavItems}
            </ul>
        </div>
    )
}

Navigation.propTypes = {
    auth: PropTypes.instanceOf(AuthService)
}


export default Navigation
