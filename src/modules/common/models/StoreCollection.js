const emptyStoreCollection = {
    "byAggregateId": {},
    "allAggregateIds": []
}

export default class StoreCollection {
    constructor(data) {
        this.data = data || emptyStoreCollection
    }

    toArray() {
        return this.data.allAggregateIds.map((aggregateId) => {
            return this.data.byAggregateId[aggregateId]
        })
    }

    has(aggregateId) {
        return this.data.allAggregateIds.indexOf(aggregateId) !== -1
    }

    find(aggregateId) {
        return this.data.byAggregateId[aggregateId]
    }

    length() {
        return this.data.allAggregateIds.length
    }

    getByIndex(index) {
        return this.data.byAggregateId[this.data.allAggregateIds[index]]
    }

    loop(eachCallback) {
        let notificationsArray = this.toArray()

        for (let index = 0; index < notificationsArray.length; index++) {
            let notification = notificationsArray[index]

            eachCallback(notification, index)
        }
    }

    map(mapCallback){
        return this.toArray().map(mapCallback)
    }

    setDataFromArray({arrayData = [], idField = 'id'}) {
        this.data = this.convertToCollectionData(arrayData, idField)
    }

    static convertToCollectionData(arrayData, idField = 'id') {
        let collectionData = {
            "byAggregateId": {},
            "allAggregateIds": []
        }

        for (let index = 0; index < arrayData.length; index++) {
            let item = arrayData[index]
            let itemId = item[idField]
            collectionData.allAggregateIds.push(itemId)
            collectionData.byAggregateId[itemId] = item
        }

        return collectionData
    }

    get data() {
        return this._data;
    }

    set data(value) {
        this._data = value;
    }
}

StoreCollection.emptyStructure = emptyStoreCollection
