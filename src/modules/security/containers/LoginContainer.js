import React, {Component} from 'react'
import {connect} from 'react-redux'
import Login from '../components/Login'
import {pushHistory} from '../../common/tools/HistoryTools'
import {getPath} from '../../common/tools/URLTools'
import ui from 'redux-ui'
import {bindActionCreators} from 'redux'
import {decodeSecurityToken} from '../redux/actions/SecurityActions'

const mapStateToProps = (state) => ({
    language: state.global.locales.lang,
    security: state.security
})

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        decodeSecurityToken,
    }, dispatch)
}

@connect(
    mapStateToProps,
    mapDispatchToProps
)
@ui({
    key: 'login',
    state: {
        error: ''
    }
})
export default class LoginContainer extends Component {

    login(values) {
        this.props.auth.login({...values, callback: ({error, data}) => {
            if (!error && this.props.auth.loggedIn()) {
                this.props.decodeSecurityToken({
                    token: data.access_token
                })
                this.props.auth.decryptedToken = this.props.auth.decryptToken({
                    token : data.access_token
                })

                setTimeout(() => pushHistory(getPath(this.props.language + '/lesson-planner')), 1000)
            } else {
                this.props.updateUI({
                    error: error.message
                })
            }
        }})
    }

    render() {
        return (
            <Login
                doLogin={this.login.bind(this)}
                errorMessage={this.props.ui.error}
            />
        )
    }
}
