import {DECODE_SECURITY_TOKEN} from '../actions/types/SecurityActionTypes'
const initialState = {}

export const auth = (state = initialState, action) => {
    switch (action.type) {
        case DECODE_SECURITY_TOKEN:
            return {
                ...state,
                ...action.payload
            }
        default:
            return state
    }
}
