import jwtDecode from 'jwt-decode'
import {DECODE_SECURITY_TOKEN} from './types/SecurityActionTypes'

function decodeSecurityTokenAction(payload) {
    return {
        type: DECODE_SECURITY_TOKEN,
        payload
    }
}


export const decodeSecurityToken = ({token}) => {
    return dispatch => {
        dispatch(decodeSecurityTokenAction(jwtDecode(token)))
    }
}
