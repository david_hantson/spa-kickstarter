import React from 'react'
import {config} from '../../../config/configureSettings'
import axios from 'axios'
import qs from 'qs'
import noop from '../../common/tools/noop'
import jwtDecode from 'jwt-decode'

let browserStorage = (typeof localStorage === 'undefined') ? null : localStorage

const redirectUrl = config.useHashRouting ? config.frontEndHost + '/#/login' : config.frontEndHost + '/login'

export default class AuthService {
    constructor(clientId, secret, host) {
        this.clientId = clientId
        this.secret = secret
        this.host = host
        this._decryptedToken = {
            authorities: []
        }
    }

    _doAuthentication(authResult) {
        this.token = authResult.access_token
        this.tokenType = authResult.token_type
        this.refreshToken = authResult.refresh_token
        this.setAxiosDefaults()
    }

    _failedAuthentication() {
        this.token = ''
        this.tokenType = ''
        this.refreshToken = ''
    }

    _doPost({url, queryParams, callback}) {
        axios.post(url, qs.stringify(queryParams), {
            crossDomain: true,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json'
            },
            auth: {
                username: this.clientId,
                password: this.secret
            }
        })
            .then(result => {
                this._doAuthentication(result.data)
                callback({data: result.data})
            })
            .catch(error => {
                this._failedAuthentication(error)
                callback({error})
            })
    }

    setAxiosDefaults() {
        axios.defaults.headers.common['Authorization'] = this.tokenType + ' ' + this.token
    }

    login({username, password, callback = noop}) {
        this._doPost({
            url: this.host + '/oauth/token',
            queryParams: {
                username,
                password,
                grant_type: 'password'
            },
            callback
        })
    }

    refresh({host, callback}) {
        this._doPost({
            url: this.host + '/oauth/token',
            queryParams: {
                client_id: this.clientId,
                client_secret: this.secret,
                grant_type: 'refresh_token',
                access_token: this.token,
                refresh_token: this.refreshToken
            },
            callback
        })
    }

    decryptToken({token}) {
        return jwtDecode(token)
    }

    hasRole({role}) {
        if (!this._decryptedToken || !this._decryptedToken.authorities) return false
        return this._decryptedToken.authorities.indexOf(role) !== -1
    }

    logout() {
        if (!!browserStorage) {
            browserStorage.removeItem('id_token')
            browserStorage.removeItem('token_type')
            browserStorage.removeItem('refresh_token')
        }
    }

    loggedIn() {
        if (!!browserStorage) return !!this.token
        else return true
    }

    get token() {
        if (!!browserStorage) return browserStorage.getItem('id_token')
    }

    set token(value) {
        if (!!browserStorage) browserStorage.setItem('id_token', value)
    }

    get tokenType() {
        if (!!browserStorage) return browserStorage.getItem('token_type')
    }

    set tokenType(value) {
        if (!!browserStorage) browserStorage.setItem('token_type', value)
    }

    get refreshToken() {
        if (!!browserStorage) return browserStorage.getItem('refresh_token')
    }

    set refreshToken(value) {
        if (!!browserStorage) browserStorage.setItem('refresh_token', value)
    }

    get decryptedToken() {
        return this._decryptedToken
    }

    set decryptedToken(value) {
        this._decryptedToken = value
    }
}
