import React, {PropTypes} from 'react'
import {Field, reduxForm} from 'redux-form'
import Button from '../../common/components/Button'
import {renderTextField} from '../../common/tools/FormTools'


const validate = values => {
    const errors = {}
    const requiredFields = [
        'username', 'password'
    ]
    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Verplicht'
        }
    })

    if (values.username && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.username)) {
        errors.username = 'Ongeldige username, vb. info@sanderus.be'
    }

    return errors
}


const LoginForm = (props) => {

    const {handleSubmit, pristine, submitting, errorMessage} = props

    return (
        <form className="gr-container" onSubmit={handleSubmit}>
            <div className="row">
                <div className="gr-4 center prefix-4 suffix-4">
                    <Field name="username" component={renderTextField} label="app.login.username"/>
                </div>
            </div>
            <div className="row">
                <div className="gr-4 center prefix-4 suffix-4">
                    <Field name="password" component={renderTextField} label="app.login.password" type="password"/>
                </div>
            </div>
            <br/>
            <div className="row">
                <div className="gr-4 center prefix-4 suffix-4">
                    <Button text={'app.login.login'}
                            type="submit"
                            disabled={pristine || submitting}
                    />
                </div>
            </div>
            <div className="row">
                <div className="gr-4 prefix-4 suffix-4">
                    <p className="error-message">{errorMessage}</p>
                </div>
            </div>
        </form>
    )
}

LoginForm.propTypes = {}

export default reduxForm({
    form: 'login',
    validate,
})(LoginForm)
