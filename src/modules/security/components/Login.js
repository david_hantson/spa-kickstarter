import React, {PropTypes} from 'react'
import LoginForm from './LoginForm'
import noop from '../../common/tools/noop'


const Login = ({
                  doLogin = noop,
                   errorMessage = ''
               }) => {
    return (
        <LoginForm
            initialValues={{
                // username: 'mishi.cooks@sanderus.com',
                // password: 'test'
            }}
            onSubmit={doLogin}
            errorMessage={errorMessage}
        />
    )
}

Login.propTypes = {}


export default Login
