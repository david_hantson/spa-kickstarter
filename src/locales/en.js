export default {
    lang: 'en',
    messages: {
        'app.header.navigation.logout': 'Logout',

        'app.footer.navigation.contact': 'Contact us',

        'app.forms.date_picker.label.ok': 'OK',
        'app.forms.date_picker.label.cancel': 'Cancel',

        'app.login.username': 'Username',
        'app.login.password': 'Password',
        'app.login.login': 'Login'
    }
}
