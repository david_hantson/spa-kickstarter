import {reducer as formReducer} from 'redux-form'
import {reducer as uiReducer} from 'redux-ui'
import global from 'modules/common/redux/reducers/GlobalReducers'
import {combineReducers} from 'redux'
import {routerReducer} from 'react-router-redux'
import {reducer as fetching} from 'redux-fetch-data'
import {security} from './modules/security/redux/reducers/index'


export default combineReducers({
    fetching,
    routing: routerReducer,
    form: formReducer,
    ui: uiReducer,
    global,
    security
})
