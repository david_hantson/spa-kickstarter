import React, {Component} from 'react'

import Footer from './modules/common/components/Footer'
import NotificationsContainer from './modules/common/containers/NotificationsContainer'
import HeaderContainer from './modules/common/containers/HeaderContainer'
import {MuiThemeProvider} from 'material-ui'
import {getMuiTheme} from 'material-ui/styles/index'
import injectTapEventPlugin from 'react-tap-event-plugin'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {switchLanguage} from './modules/common/redux/actions/LocalesActions'
import {getLanguageFromURL} from './modules/common/tools/Internationalization'
import {pushHistory} from './modules/common/tools/HistoryTools'
injectTapEventPlugin()

const getTheme = ({userAgent}) => {
    return getMuiTheme({
        userAgent: userAgent,
        palette: {
            primary1Color: '#004a9f',
            primary2Color: '#004a9f'
        },
        radioButton: {
            borderColor: '#c9c9c9',
            // backgroundColor: palette.alternateTextColor,
            // checkedColor: palette.primary1Color,
            // requiredColor: palette.primary1Color,
            // disabledColor: palette.disabledColor,
            // size: 24,
            // labelColor: palette.textColor,
            // labelDisabledColor: palette.disabledColor,
        },
        timePicker: {
            // color: palette.alternateTextColor,
            // textColor: palette.alternateTextColor,
            accentColor: '#ec7814',
            // clockColor: palette.textColor,
            // clockCircleColor: palette.clockCircleColor,
            headerColor: '#004a9f',
            // selectColor: palette.primary2Color,
            // selectTextColor: palette.alternateTextColor,
        },
        menuItem: {
            // dataHeight: 32,
            // height: 48,
            // hoverColor: fade(palette.textColor, 0.1),
            // padding: spacing.desktopGutter,
            selectedTextColor: '#ec7814',
            // rightIconDesktopFill: grey600,
        }
    })
}

const mapStateToProps = (state) => {
    return {}
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        switchLanguage
    }, dispatch)
}

@connect(
    mapStateToProps,
    mapDispatchToProps
)
class App extends Component {
    logout() {
        // destroys the session data
        this.props.route.auth.logout()
        // redirects to login page
        pushHistory('/')
    }

    render() {
        let children = null
        if (this.props.children) {
            if (this.props.route.auth.loggedIn()) {
                // this.props.route.auth.
                this.props.route.auth.decryptedToken = this.props.route.auth.decryptToken({
                    token: this.props.route.auth.token
                })
            }

            children = React.cloneElement(this.props.children, {
                auth: this.props.route.auth //sends auth instance from route to children
            })
        }

        let muiTheme = getTheme({userAgent: this.props.userAgent})

        return (
            <MuiThemeProvider muiTheme={muiTheme}>
                <div className="main-container">
                    <div className="page-wrapper">
                        <HeaderContainer logout={this.logout.bind(this)} auth={this.props.route.auth}/>
                        <NotificationsContainer/>
                        {children}
                    </div>
                    <Footer />
                </div>
            </MuiThemeProvider>
        )
    }
}

export default App
