import glob from 'glob'
// import {default as commonConfig} from '../../src/config/common.json'

export default class StubServer {
    static start(app) {
        // global.commonConfig = {...commonConfig};

        let routerPath = './server/stub/routes/**/*';

        glob.sync(routerPath).map(function (file) {
            console.log('INCLUDING ' + file);
            require(file.replace('/server/stub', ''))(app);
        });
    }

}
