// import {getStubURL} from '../../../src/modules/common/tools/URLTools'

module.exports = function (app) {
    app.get('/api/v1/version', function (req, res) {
        res.send({
            'version': '1.0'
        })
    })

    // app.route(getStubURL('getStoreCollectionExample'))
    //     .get(function (req, res) {
    //         res.send(require('../../../test/data/example.json'))
    //     })
}
