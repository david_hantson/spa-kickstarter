import React from 'react'
import {expect} from 'chai'
import {mock, spy} from 'sinon'
import StoreCollection from '../../../../src/modules/common/models/StoreCollection'

describe('Scenario: StoreCollection', () => {
    const storeCollection = new StoreCollection({
        "byAggregateId": {
            "9306a106-2343-4a2e-943c-724ecbf6f402": {
                "referenceId": "12345",
                "status": "INACTIVE"
            },
            "94cce59e-3f45-4f6a-bc29-d3594577e601": {
                "referenceId": "67891",
                "status": "ACTIVE"
            },
            "92bbd48e-2e34-3e5a-ab18-e2483466d590": {
                "referenceId": "23456",
                "status": "ACTIVE"
            }
        },
        "allAggregateIds": [
            "9306a106-2343-4a2e-943c-724ecbf6f402",
            "94cce59e-3f45-4f6a-bc29-d3594577e601",
            "92bbd48e-2e34-3e5a-ab18-e2483466d590"
        ]
    })
    describe('Given I create an instance of StoreCollection, where the second of 3 panelists in the collection ' +
        'has aggregateId "94cce59e-3f45-4f6a-bc29-d3594577e601"', () => {
        const panelistAggregateId = '9306a106-2343-4a2e-943c-724ecbf6f402'

        describe('When I want to collect all panelists it', () => {
            it('Should be possible to collect the panelists in an array', () => {
                expect(storeCollection.toArray()).to.be.an('array')
            })
            it('Should have 3 panelists in the collection', () => {
                expect(storeCollection.toArray().length).to.equal(3)
            })
        })
        describe('When I want to check if the second panelist exists and find it, it', () => {
            const panelistWithAggregateId = storeCollection.data.byAggregateId[panelistAggregateId];

            it('Should be possible to check by aggregateId if the collection holds a specific panelist', () => {
                expect(storeCollection.has(panelistAggregateId)).to.be.true
            })
            it('Should be possible to find a specific panelist by aggregateId', () => {
                expect(storeCollection.find(panelistAggregateId)).to.deep.equal(
                    panelistWithAggregateId
                )
            })
        })
    })
    describe('Given I create an instance of StoreCollection, which does not hold any panelists', () => {
        let storeCollection = new StoreCollection()

        describe('When I call the function to collect the (non-existing) panelists it', () => {
            it('Should use the default data', () => {
                expect(storeCollection.data).to.deep.equal({
                    "byAggregateId": {},
                    "allAggregateIds": []
                })
            })
            it('Should return an empty array', () => {
                expect(storeCollection.toArray().length).to.be.equal(0)
            })
        })
        describe('When I want to search for panelists it', () => {
            it('Should return false if you check for a panelist to exist', () => {
                expect(storeCollection.has('12345')).to.be.false
            })
            it('Should return undefined', () => {
                expect(storeCollection.find('12345')).to.be.undefined
            })
        })
    })
})
