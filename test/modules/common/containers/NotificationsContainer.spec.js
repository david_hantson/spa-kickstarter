import {shallow} from 'enzyme'
import NotificationsContainer from '../../../../src/modules/common/containers/NotificationsContainer'
import React from 'react'
import configureMockStore from 'redux-mock-store'
import {stub} from 'sinon'
import {expect} from 'chai'
import StoreCollection from '../../../../src/modules/common/models/StoreCollection'


describe('Scenario: render a NotificationContainer', () => {
    const mockStore = configureMockStore()

    describe('Given I have a message of type ERROR' +
        '   And no duration', () => {
        let Container, stubActions

        const fakeGlobalStore = {
            global: {
                notifications: {
                    byAggregateId: {
                        'f56e2240-5bc2-4647-b74c-711a0f0424c0' : {
                            "id": "f56e2240-5bc2-4647-b74c-711a0f0424c0",
                            "isClosing": false,
                            "type": "ERROR",
                            "text": "Something went wrong, fix it fast please",
                            "duration": 10
                        }
                    },
                    allAggregateIds: ['f56e2240-5bc2-4647-b74c-711a0f0424c0']
                }
            }
        }

        const notificationsCollection = new StoreCollection(fakeGlobalStore.global.notifications)

        let props = {
            notifications: notificationsCollection
        }

        beforeEach(() => {
            let store = mockStore(fakeGlobalStore)

            stubActions = {
                startClosingNotification: stub(),
                removeNotification: stub()
            }

            Object.assign(props, {
                store,
                ...stubActions,

            })

            let wrapper = shallow(<NotificationsContainer {...props}/>)
            Container = wrapper.instance()
        })

        describe('When it is rendered it', () => {
            it('should render the container', () => {
                expect(Container).to.exists
            })

            it('should contain 1 Notification component')
        })

        describe('When the notification has just been added it', () => {

            it('should update the Timers')
        })

        describe('When the timers are updated it', () => {

            it('should not add a "remove notification timer"')
        })

        describe('When the notification is removed it', () => {

            it('should call the action to remove the notification with the id as parameter')

            it('should call the method to remove the id from the timers')
        })

    })

    describe('Given I have a message of type SUCCESS ' +
        '   And a duration of 25 seconds', () => {
        let Container
        let props = {}

        const fakeGlobalStore = {
            global: {
                notifications: {
                    byAggregateId: {},
                    allAggregateIds: ['0d01a597-c650-4551-b1d4-97c249341bb9']
                }
            }
        }

        beforeEach(() => {
        })

        describe('When it is rendered', () => {
            it('should render the container')
        })

        describe('When the timers are updated it', () => {

            it('should add a "remove notification timer"')
        })

        describe('When a new "remove notification timer" is added it', () => {

            it('should add the notification id to the ui state timers')

            it('should close the notification after the timer has ended and the animation ended')
        })

        describe('When a notification is removed from the timers state it', () => {

            it('should update the ui state without the notification id in the timers array')
        })
    })

})

describe('Scenario: connecting NotificationContainer to the redux store', () => {
    //TODO: mapStateToProps
    //TODO: mapDispatchToProps
})
