import React from 'react'
import {shallow} from 'enzyme'
import {expect} from 'chai'
import {stub} from 'sinon'
import Notification from '../../../../src/modules/common/components/Notification'
import {INFO} from '../../../../src/modules/common/components/NotificationTypes'

describe('Scenario: render a <Notification />', () => {

    describe('Given I have a message type INFO, ' +
        '   And with a unique id' +
        '   And some text' +
        '   And duration of 15 seconds' +
        '   And a close function', () => {

        let Component
        let props = {
            id: '1552dd4a-7471-4555-8f96-2cd0035953e9',
            type: INFO,
            text: 'some information',
            duration: 15
        }

        describe('When a new notification is rendered it', () => {

            beforeEach(() => {
                props.onClose = stub()
                Component = shallow(<Notification {...props}/>)
            })

            it('should render', () => {
                expect(Component).to.exist
            })

            it('should call the onClose method with the message id', () => {
                Component.find('.close-icon').simulate('click')
                expect(props.onClose.firstCall.args[0]).to.deep.equal({
                    id: props.id
                })
            })

            it('should have a class with name duration-15', () => {
                expect(Component.find('.message').hasClass('duration-15')).to.be.true
            })

            it('should have a class with name timed', () => {
                expect(Component.find('.message').hasClass('timed')).to.be.true
            })

        })

        describe('When the notification is closing it', () => {

            beforeEach(() => {
                props.isClosing = true
                Component = shallow(<Notification {...props}/>)
            })

            it('should have a class with name is-closing', () => {
                expect(Component.find('.message').hasClass('is-closing')).to.be.true
            })

            it('should no longer have a class duration-15', () => {
                expect(Component.find('.message').hasClass('duration-15')).to.be.false
            })

            it('should still have the class timed', () => {
                expect(Component.find('.message').hasClass('timed')).to.be.true
            })
        })

    })
})