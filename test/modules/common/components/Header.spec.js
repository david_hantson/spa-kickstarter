import React from 'react'
import {shallow} from 'enzyme'
import {expect} from 'chai'
import {stub} from 'sinon'
import {Header} from '../../../../src/modules/common/components/Header'

describe('Scenario: render a <Header />', () => {

    describe('Given I have a Header', () => {

        let Component
        let props = {}

        describe('When the Header is rendered it', () => {

            beforeEach(() => {

                Component = shallow(<Header {...props}/>)
            })

            it('should render', () => {
                expect(Component).to.exist
            })
        })
    })
})