import React from 'react'
import {shallow} from 'enzyme'
import {expect} from 'chai'
import {stub} from 'sinon'
import {Icon, Stack} from '../../../../src/modules/common/components/Icons'

describe('Scenario: render a <Icon />', () => {

    describe('Given I have a Icon' +
        '   And no properties', () => {

        let Component
        let props = {}

        describe('When the Icon is rendered it', () => {

            beforeEach(() => {
                Component = shallow(<Icon {...props}/>)
            })

            it('should render', () => {
                expect(Component).to.exist
            })
        })
    })

    describe('Given I have a Icon' +
        '   And stackSize 2', () => {

        let Component
        let props = {
            stackSize: 2
        }

        describe('When the Icon is rendered it', () => {

            beforeEach(() => {
                Component = shallow(<Icon {...props}/>)
            })

            it('should render', () => {
                expect(Component).to.exist
            })

            it('should have a class with name "fa-stack-2x"', () => {
                expect(Component.hasClass('fa-stack-2x')).to.be.true
            })
        })
    })

    describe('Given I have a Icon' +
        '   And with className "my-icon"' +
        '   And with name "user"' +
        '   And with size "lg"' +
        '   And with fixedWidth set to true' +
        '   And with inverse set to true', () => {

        let Component
        let props = {
            className: 'my-icon',
            name: 'user',
            size: 'lg',
            fixedWidth: true,
            inverse: true
        }

        describe('When the Icon is rendered it', () => {

            beforeEach(() => {
                Component = shallow(<Icon {...props}/>)
            })

            it('should render', () => {
                expect(Component).to.exist
            })

            it('should have a class with name "my-icon"', () => {
                expect(Component.hasClass('my-icon')).to.be.true
            })

            it('should have a class with name "fa-user"', () => {
                expect(Component.hasClass('fa-user')).to.be.true
            })

            it('should have a class with name "fa-lg"', () => {
                expect(Component.hasClass('fa-lg')).to.be.true
            })

            it('should have a class with name "fa-fw"', () => {
                expect(Component.hasClass('fa-fw')).to.be.true
            })

            it('should have a class with name "fa-inverse"', () => {
                expect(Component.hasClass('fa-inverse')).to.be.true
            })
        })
    })
})

describe('Scenario: render a <Stack />', () => {

    describe('Given I have a Stack' +
        '   And with size "1x"', () => {

        let Component
        let props = {
            size: '1x'
        }

        describe('When the Stack is rendered it', () => {

            beforeEach(() => {
                Component = shallow(
                    <Stack {...props}>
                        <Icon/>
                        <Icon/>
                    </Stack>)
            })

            it('should render', () => {
                expect(Component).to.exist
            })

            it('should have a class with name "fa-stack"', () => {
                expect(Component.hasClass('fa-stack')).to.be.true
            })

            it('should have a class with name "fa-1x"', () => {
                expect(Component.hasClass('fa-1x')).to.be.true
            })

            it('should render the child icons with properties stackSize 2 and 1', () => {
                expect(Component.find(Icon).length).to.equals(2)
                expect(Component.find(Icon).nodes[0].props).to.have.property('stackSize')
                expect(Component.find(Icon).nodes[0].props.stackSize).to.equals(2)
                expect(Component.find(Icon).nodes[1].props).to.have.property('stackSize')
                expect(Component.find(Icon).nodes[1].props.stackSize).to.equals(1)
            })
        })
    })

    describe('Given I have a Stack' +
        '   And no properties', () => {

        let Component
        let props = {
        }

        describe('When the Stack is rendered it', () => {

            beforeEach(() => {
                Component = shallow(
                    <Stack {...props}>
                        <Icon/>
                        <Icon/>
                    </Stack>)
            })

            it('should render', () => {
                expect(Component).to.exist
            })
        })
    })
})