import React from 'react'
import {shallow} from 'enzyme'
import {expect} from 'chai'
import {stub} from 'sinon'
import Footer from '../../../../src/modules/common/components/Footer'

describe('Scenario: render a <Footer />', () => {

    describe('Given I have a footer', () => {

        let Component
        let props = {}

        describe('When the footer is rendered it', () => {

            beforeEach(() => {
                Component = shallow(<Footer {...props}/>)
            })

            it('should render', () => {
                expect(Component).to.exist
            })
        })
    })
})