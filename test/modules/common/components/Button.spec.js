import React from 'react'
import {shallow} from 'enzyme'
import {expect} from 'chai'
import {stub} from 'sinon'
import {ButtonTypes} from '../../../../src/modules/common/components/Button'
import Button from '../../../../src/modules/common/components/Button'

describe('Scenario: render a <Button />', () => {

    describe('Given I have a primary button, ' +
        '   And with text "Click"' +
        '   And a class custom-button' +
        '   And an onClick action', () => {

        let Component
        let props = {
            styleType: ButtonTypes.PRIMARY,
            text: 'Click',
            className: 'custom-button'
        }

        describe('When the button is rendered it', () => {

            beforeEach(() => {
                Component = shallow(<Button {...props}/>)
            })

            it('should render', () => {
                expect(Component).to.exist
            })

            it('should have text "Click"', () => {
                expect(Component.node.props.label).to.equal('Click')
            })

            it('should have a class named "button"', () => {
                expect(Component.hasClass('button')).to.be.true
            })

            it('should have a class named "primary"', () => {
                expect(Component.hasClass('primary')).to.be.true
            })

            it('should have a class named "custom-button"', () => {
                expect(Component.hasClass('custom-button')).to.be.true
            })
        })

        describe('When the button is clicked it', () => {

            beforeEach(() => {
                props.onClick = stub()
                Component = shallow(<Button {...props}/>)
            })

            it('should perform the onClick action', () => {
                Component.simulate('click')
                expect(props.onClick.calledOnce).to.be.true
            })
        })

    })

    describe('Given I have a secondary button, ' +
        '   And with text "Second"' +
        '   And with a class second-button' +
        '   And an onClick action', () => {

        let Component
        let props = {
            styleType: ButtonTypes.SECONDARY,
            text: 'Second',
            className: 'second-button'
        }

        describe('When the button is rendered it', () => {

            beforeEach(() => {
                Component = shallow(<Button {...props}/>)
            })

            it('should render', () => {
                expect(Component).to.exist
            })

            it('should have text "Second"', () => {
                expect(Component.node.props.label).to.equal('Second')
            })

            it('should have a class named "button"', () => {
                expect(Component.hasClass('button')).to.be.true
            })

            it('should have a class named "secondary"', () => {
                expect(Component.hasClass('secondary')).to.be.true
            })

            it('should have a class named "second-button"', () => {
                expect(Component.hasClass('second-button')).to.be.true
            })
        })

        describe('When the button is clicked it', () => {

            beforeEach(() => {
                props.onClick = stub()
                Component = shallow(<Button {...props}/>)
            })

            it('should perform the onClick action', () => {
                Component.simulate('click')
                expect(props.onClick.calledOnce).to.be.true
            })
        })
    })

    describe('Given I have button, ' +
        '   And no properties', () => {

        let Component

        describe('When the button is rendered it', () => {

            beforeEach(() => {
                Component = shallow(<Button />)
            })

            it('should render', () => {
                expect(Component).to.exist
            })
        })
    })
})
