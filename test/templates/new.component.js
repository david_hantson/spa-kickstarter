import React from 'react'
import {shallow} from 'enzyme'
import {expect} from 'chai'
import {stub} from 'sinon'

describe('Scenario: render a <${NAME} />', () => {

    describe('Given I have a ${NAME}' +
        '   And with no properties', () => {

        let Component
        let props = {}

        describe('When the ${NAME} is rendered it', () => {

            beforeEach(() => {
                Component = shallow(<${NAME} {...props}/>)
            })

            it('should render', () => {
                expect(Component).to.exist
            })
        })
    })
})