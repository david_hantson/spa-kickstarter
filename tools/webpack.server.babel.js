import fs from 'fs'
import path from 'path'
import webpack from 'webpack'
import deepmerge from 'deepmerge'
import webpackCommonConfig from './webpack.common.babel'

let nodeModules = {}
fs.readdirSync('node_modules')
    .filter(function (x) {
        return ['.bin'].indexOf(x) === -1
    })
    .forEach(function (mod) {
        nodeModules[mod] = 'commonjs ' + mod
    })

let sourceMapSupportModule = 'require(\'source-map-support\').install({environment: \'node\'});\n\n'

let loaders = webpackCommonConfig.module.loaders.concat()
loaders.push({test: /\.scss$/, loader: 'null-loader'})

delete webpackCommonConfig.module

export default deepmerge({
    devtool: 'source-map',
    entry: [
        // 'webpack/hot/poll?1000',
        './server.babel.js'
    ],
    output: {
        path: path.join(process.cwd(), 'tmp'),
        filename: 'bundle.js'
    },
    // externals: [ nodeExternals({ whitelist: [ "webpack/hot/poll?1000" ] }) ],
    target: 'node',
    module: {
        loaders: loaders
    },
    plugins: [
        new webpack.BannerPlugin({
            banner: sourceMapSupportModule,
            raw: true,
            entryOnly: true
        }),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.DefinePlugin({
            __CLIENT__: false,
            __SERVER__: true,
            __PRODUCTION__: false,
            __DEV__: true,
            'process.env.NODE_ENV': '"' + process.env.NODE_ENV + '"'
        }),
        new webpack.IgnorePlugin(/vertx/)
    ],
    externals: nodeModules
}, webpackCommonConfig)
