import React from 'react'
import {renderToString} from 'react-dom/server'
import {match, RouterContext} from 'react-router'
import {createStore, applyMiddleware} from 'redux'
import {Provider} from 'react-redux'
import {fetchDataOnServer} from 'redux-fetch-data'
import {AppContainer} from 'react-hot-loader'
import reducers from '../src/reducers'
import createHistory from 'react-router/lib/createMemoryHistory'
import ConnectedIntlProvider from '../src/modules/common/containers/ConnectedIntlProvider'
import {config} from '../src/config/configureSettings'
import thunk from 'redux-thunk'
import setupReact from '../src/setup'
import {defaultLanguage, polyfillIntl} from '../src/modules/common/tools/Internationalization'

polyfillIntl()

let webpackDevServer = {}
webpackDevServer.hostname = process.env.WP_HOST || 'localhost'
webpackDevServer.port = process.env.WP_PORT || config.assetPort || 8079
webpackDevServer.path = 'http://' + webpackDevServer.hostname + ':' + webpackDevServer.port

export const assetMiddleware = () => {
    return (req, res, next) => {
        let mainjs = 'main.js',
            maincss = 'main.css'

        switch (process.env.NODE_ENV) {
            case 'development':
                res.locals.pretty = true
                res.locals.app_stylesheets = '\n      <script src=\'' + webpackDevServer.path + '/assets/js/devServerClient.js\'></script>\n      <script src=\'' + webpackDevServer.path + '/assets/js/' + mainjs + '\'></script>'
                res.locals.app_scripts = '\n      <script src=\'' + webpackDevServer.path + '/assets/js/plugins.js\'></script>\n      <script src=\'' + webpackDevServer.path + '/assets/js/app.js\'></script>'
                break
            case 'production' :
                res.locals.app_stylesheets = '\n      <link rel=\'stylesheet\' href=\'/css/' + maincss + '\' />'
                res.locals.app_scripts = '\n      <script src=\'/js/plugins.js\'></script>\n      <script src=\'/js/app.js\'></script>'
                break
        }

        next()
    }
}


export const renderHTMLString = (routes, req, callback) => {
    setupReact()
    const history = createHistory(req.originalUrl)
    const requestedLanguage = req.path.replace(config.basePath, '').split('/')[0] || defaultLanguage
    const store = createStore(reducers, {
        global: {
            locales: {
                lang: requestedLanguage,
                messages: require('../src/locales/' + requestedLanguage + '.js').default.messages
            }
        }
    }, applyMiddleware(thunk))

    match({routes, location: req.url}, (error, redirect, renderProps) => {
        if (error) {
            callback(error)
        } else if (redirect) {
            callback(null, redirect)
        } else if (renderProps) {
            fetchDataOnServer(renderProps, store).then(() => {
                let content

                try {
                    let language = renderProps.location.pathname.replace(config.basePath, '').split('/')[0]

                    content = renderToString(
                        <AppContainer>
                            <Provider store={store}>
                                <ConnectedIntlProvider>
                                    <RouterContext
                                        {...renderProps}
                                        createElement={(Component, props) => {
                                            return (
                                                <Component
                                                    {...props}
                                                    userAgent={req.headers['user-agent']}
                                                    language={language}
                                                />
                                            )
                                        }}
                                    />
                                </ConnectedIntlProvider>
                            </Provider>
                        </AppContainer>
                    )
                    callback(null, null, {
                        content: content,
                        data: store.getState()
                    })
                } catch (err) {
                    callback(null, null, {
                        content: '',
                        // data: store.getState()
                    })
                    // callback({
                    //     message: 'something went wrong during render: ' + err.stack
                    // })
                }
            })
        }
        else {
            callback({
                message: 'Not found'
            })
        }
    })
}
